function userDetail(type, typeId) {
	$('#modal-title').html('');
	$('#modal-body').html('');
	console.log("userDetail: "+type+", "+typeId);
	if (typeId === 'New') {
		$('#btnDelete').css('display', 'none');
	}
	$.ajax({
		url: 'User/userDetail.php',
		data: {
			type: type,
			typeId: typeId
		},
		success: function(data) {
			console.log('success');
			$('#modal-body').html(data);
			$('#modal-title').html(type);
			$('#modalDetail').modal('show');
		}
	});
}
