<?php
	if (!$_SESSION) {session_start();}
	if ($_SESSION['loggedIntoVAdminBackEnd'] === "UserHasSuccessfullyLoggedInToVAdminBackEnd" && $_SESSION['token'] === session_id()) {
		$LoggedUser = $_SESSION['LoggedUser'];
		echo "<br>In Session!<br>";
		include_once '../config.php';
		include_once DIR_INC . "conn.inc.php";
		include_once DIR_CLASSES . 'classUserDisplay.php';
		$User = new classUserDisplay($LoggedUser);
?>
<p>User Detail</p>
<?php 
	}
	else {
		session_destroy();
		header('location: index.php');
	}
?>
