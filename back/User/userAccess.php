<?php
	if (!$_SESSION) {session_start();}
	if ($_SESSION['loggedIntoVAdminBackEnd'] === "UserHasSuccessfullyLoggedInToVAdminBackEnd" && $_SESSION['token'] === session_id()) {
		$LoggedUser = $_SESSION['LoggedUser'];
//		include_once '../config.php';
		include_once DIR_INC . "conn.inc.php";
		include_once DIR_CLASSES . 'classUserDisplay.php';
		$User = new classUserDisplay($LoggedUser);
?>
<script src="User/scripts/users.min.js" type="text/javascript"></script>
<script>
	$(document).ready(function() {
		setActive('userAccess');
		$('.filterable .btn-filter').click(function(){
			var $panel = $(this).parents('.filterable'),
			$filters = $panel.find('.filters input'),
			$tbody = $panel.find('.table tbody');
			if ($filters.prop('disabled') == true) {
				$filters.prop('disabled', false);
				$filters.first().focus();
			} else {
				$filters.val('').prop('disabled', true);
				$tbody.find('.no-result').remove();
				$tbody.find('tr').show();
			}
		});

		$('.filterable .filters input').keyup(function(e){
			/* Ignore tab key */
			var code = e.keyCode || e.which;
			if (code == '9') return;
			/* Useful DOM data and selectors */
			var $input = $(this),
			inputContent = $input.val().toLowerCase(),
			$panel = $input.parents('.filterable'),
			column = $panel.find('.filters th').index($input.parents('th')),
			$table = $panel.find('.table'),
			$rows = $table.find('tbody tr');
			/* Dirtiest filter function ever ;) */
			var $filteredRows = $rows.filter(function(){
				var value = $(this).find('td').eq(column).text().toLowerCase();
				return value.indexOf(inputContent) === -1;
			});
			/* Clean previous no-result if exist */
			$table.find('tbody .no-result').remove();
			/* Show all rows, hide filtered ones (never do that outside of a demo ! xD) */
			$rows.show();
			$filteredRows.hide();
			$('#RowCount').html("   ("+($rows.length-$filteredRows.length)+" rows)");
			/* Prepend no-result row if all rows are filtered */
			if ($filteredRows.length === $rows.length) {
				$table.find('tbody').prepend($('<tr class="no-result text-center"><td colspan="'+ $table.find('.filters th').length +'">No result found</td></tr>'));
			}

		});
	});

</script>
	<h4 class="text-center">User Access</h4>
	<div class="panel panel-va filterable">
		<div class="panel-heading">
			<h4>
				<button class="btn btn-default btn-xs btn-filter"><span class="glyphicon glyphicon-filter"></span> Filter</button>&nbsp;&nbsp;&nbsp;
				Users&nbsp;&nbsp;&nbsp;
				<button class="btn btn-danger btn-xs" onclick="userDetail('user', 'New')">Add</button>
			</h4>
			<div>
			</div>

		</div>

		<div id="PlanList" class="container-fluid">
		<?php 	$User->showUserAccessList(); ?>
		</div>
	</div>		

<?php 
	}
	else {
		session_destroy();
		header('location: index.php');
	}
?>
