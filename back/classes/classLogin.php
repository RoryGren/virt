<?php
include DIR_INC . 'traitCrypt.php';
/**
 * Description of classLogin
 *
 * @author rory
 */
class classLogin extends mysqli {
	use traitCrypt;

	private $validLogin;
	private $loggedUserId;
	
	public function __construct($userName, $password) {
		$this->validLogin = FALSE;
		$this->checkLogin($userName, $password);
	}
	
	private function checkLogin($userName, $password) {
		$this->validLogin = TRUE;
		$this->loggedUserId = 1;
	}
	
	public function checkValidLogin() {
		return $this->validLogin;
	}
	
	public function getLoggedUser() {
		return $this->loggedUserId;
	}
}

?>
