<?php
/**
 * Description of classPlan
 *
 * @author rory
 */
class classPlan extends MYSQLi {

	protected $PlanRoomIdList;
	protected $PlanRoomDataList;
	protected $PlanList;
	protected $PrettyHeaders = array();
	private $ZoneList = array();
	private $StyleList = array();
//	protected $ZoneHeaders;
	private $ZoneCategories = array();
	private $GeneralParams;
//	protected $ZoneCatHeaders;

	public function __construct() {
		$this->connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		$this->fetchPlanList();
		$this->fetchMasters();
	}
	
	protected function getStyles() {
		return $this->StyleList;
	}
	
	protected function getZoneList() {
		return $this->ZoneList;
	}
	
	protected function getZoneCategories() {
		return $this->ZoneCategories;
	}
	
	protected function getPrettyHeaders() {
		return $this->PrettyHeaders;
	}
	
	protected function getGeneralParams() {
		return $this->GeneralParams;
	}
	
	protected function findPlanRooms($PlanId) {
		// =================================
		// Get Room ID list from Plan record
		// =================================
		$SQL = "SELECT `RoomIdList` FROM `Plan` WHERE `PlanId` = $PlanId";
		$query = $this->query($SQL);
		$row = mysqli_fetch_assoc($query);
		$this->PlanRoomIdList = $row['RoomIdList'];
		mysqli_free_result($query);
		// ==========================================
		// Get array of Rooms with length and breadth
		// ==========================================
		$RoomIdArray = explode(',', $this->PlanRoomIdList);
		$SQL = "SELECT `zoneId`, `zoneDesc`, `zoneCategoryId`, `Length`, `Breadth` FROM `Zone` WHERE `zoneId` in ($this->PlanRoomIdList)";
		$query = $this->query($SQL);
		$RoomData = array();
		$SelectedRoomData = array();
		while ($row = mysqli_fetch_assoc($query)) {
			array_push($RoomData, $row);
		}
		mysqli_free_result($query);
		// ===============================================
		// Import room data into room list by searchableId
		// ===============================================
		foreach ($RoomIdArray as $key => $zoneId) {
			foreach ($RoomData as $dataKey => $data) {
				if ($data['zoneId'] == $zoneId) {
					$SelectedRoomData[$key] = $data;
				}
			}
		}
		$this->PlanRoomDataList = $SelectedRoomData;
	}
	
	private function fetchPlanList() {
		$SQL = "SELECT `PlanId`, `PlanStyleId`, `Code`, `Description`, `RoomIdList`, `TotalArea` FROM `Plan`";
		$this->PlanList = array();
		$result = $this->query($SQL);
		while($row = mysqli_fetch_assoc($result)) {
			array_push($this->PlanList, $row);
		}
		mysqli_free_result($result);
	}
	
	private function fetchMasters() {
//		===== Styles =====
		$SQLStyles = "SELECT `styleId`, `styleDesc`, `styleCode`, `isActive` FROM `Style`";
		$result = $this->query($SQLStyles);
		while($row = mysqli_fetch_assoc($result)) {
			$this->StyleList[$row['styleId']] = $row;
		}
		mysqli_free_result($result);
//		===== Zones =====
		$SQLZones = "SELECT `zoneId`, `zoneDesc`, `zoneCategoryId`, `Length`, `Breadth`, `isActive`, `viewBy` FROM `Zone` ORDER BY `viewBy`";
		$result = $this->query($SQLZones);
		while($row = mysqli_fetch_assoc($result)) {
			$this->ZoneList[$row['zoneId']] = $row;
		}
		mysqli_free_result($result);
//		===== Zone Categories =====
		$SQLCats  = "SELECT `catId`, `catDesc`, `catCode`, `isActive` FROM `ZoneCategory`";
		$result = $this->query($SQLCats);
		while($row = mysqli_fetch_assoc($result)) {
			$this->ZoneCategories[$row['catId']] = $row;
		}
		mysqli_free_result($result);
//		===== Pretty Headers =====
		$SQLHeaders = "SELECT `Field`, `Header` FROM `PrettyHeaders`";
		$result = $this->query($SQLHeaders);
		while($row = mysqli_fetch_assoc($result)) {
			$this->PrettyHeaders[$row['Field']] = $row['Header'];
		}
		mysqli_free_result($result);
//		===== General Parameters =====
		$sql = "SELECT `ParamName`, `ParamDesc`, `ParamVal` FROM `GeneralParam`";
		$result = $this->query($sql);
		while ($row = mysqli_fetch_array($result)) {
			$this->GeneralParams[$row[0]] = array($row[1], $row[2]);
		}
		mysqli_free_result($result);
	}

	
}

?>



