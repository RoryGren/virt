<?php
include_once DIR_INC . 'traitUser.php';
/**
 * Description of classPlan
 *
 * @author rory
 */
class classUser extends MYSQLi {
	use traitUser;
	protected $LoggedUserId;
	protected $SelectededUserId;
	protected $UserLoginList;
	protected $UserModules;
	protected $PrettyHeaders = array();

	public function __construct() {
		$this->connect(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		$this->fetchUserLogins();
	}
	
	protected function getUserLogins() {
		return $this->UserLoginList;
	}
	
	protected function getUserModuleList() {
		return $this->UserModules;
	}
	
	protected function getPrettyHeaders() {
		return $this->PrettyHeaders;
	}
	
	private function fetchUserLogins() {
		$sql = "SELECT `id`, `logInId`, `accessLevel`, `activeStatus` FROM `VUAC`";
		$result = $this->query($sql);
		while($row = mysqli_fetch_assoc($result)) {
			$this->UserLoginList[$row['id']] = $row;
		}
		mysqli_free_result($result);
	}
	
	private function fetchModuleList() {
		$sql = "SELECT `id`, `Code`, `Description` FROM `Module` ORDER BY `Description`";
		$result = $this->query($sql);
		while ($row = mysqli_fetch_assoc($result)) {
			$this->UserModules[$result['id']] = $result;
		}
		mysqli_free_result($result);
	}
}

?>



