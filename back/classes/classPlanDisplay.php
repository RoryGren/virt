<?php
include DIR_CLASSES . '/classPlan.php';
/**
 * Description of classPlanDisplay
 *
 * @author rory
 */
class classPlanDisplay extends classPlan {

	private $LoggedUser;
	
	public function __construct() {
		parent::__construct();
	}
	
	public function showGeneralParams() {
		$GeneralParams = $this->getGeneralParams();
		$tableTop    = "<table class='table'>";
		$tableHeader = "<thead><tr><th>Parameter:</th><th>Value:</th><th></th></tr></thead>";
		$tableBody   = "<tbody>";
		$tableFooter = "</tbody></table>";
		foreach ($GeneralParams as $code => $params) {
			$tableBody .= "<tr>"
					. "<td>" . $params[0] . "</td>"
					. "<td class='saveInputs'>"
					. "<input type='hidden' id='old_$code' value='" . $params[1] . "'>"
					. "<input type='text' class='text-right' id='$code' value='" . $params[1] . "'>"
					. "</td>"
					. "<td class='saveButtons' id='btn$code'>"
					. "<button class='btn btn-danger  hide' id='btnS$code' onclick='saveParam(\"$code\")'>Save Change</button>&nbsp;&nbsp;"
					. "<button class='btn btn-success hide' id='btnC$code' onclick='cancelParam(\"$code\")'>Cancel Change</button>"
					. "</td></tr>";
		}
		echo $tableTop . $tableHeader . $tableBody . $tableFooter;
	}

	public function showPlanList() {
		$tableTop    = "<table class='table table-bordered table-hover' id='plan'>";
		$tableHeader = "<thead><tr class='filters'>"
				. "<th><input type='text' class='form-control' placeholder='Code' disabled></th>"
				. "<th><input type='text' class='form-control' placeholder='Description' disabled></th>"
				. "<th><input type='text' class='form-control' placeholder='Total Area' disabled></th></tr></thead>";
		$tableBody   = "<tbody>";
		$tableFooter = "</tbody></table>";
		foreach ($this->PlanList as $key => $data) {
//			Array ( [PlanId] => 1 [PlanStyleId] => 1 [Code] => E265-3-2.5 [Description] => Farm Style 3B/R 2½ Bathrooms [RoomIdList] => 1,3,4,6,14,14,14 [TotalArea] => 265 ) 
			$tableBody .= "<tr type='button' id='" . $data['PlanId'] . "'>";
			$tableBody .= "<td>" . $data['Code'] . "</td>";
			$tableBody .= "<td>" . $data['Description'] . "</td>";
			$tableBody .= "<td>" . $data['TotalArea'] . "</td>";
			$tableBody .= "</tr>";
		}
		echo $tableTop . $tableHeader . $tableBody . $tableFooter;
	}
	
	public function showPlanDetail($planId) {
		if ($planId !== 'New') {
			
		}
	}
	
	private function getSelect($selId, $listArray, $selectedItemId, $valuePosition) { // valuePosition = array key
		$selTop = "<select id='$selId'>";
		$selFooter = "</select>";
		$selBody = "";
		foreach ($listArray as $key => $value) {
			$selBody .= "<option value='" . $value[0] . "'";
			if ($selectedItemId === $value[0]) {
				$selBody .= " SELECTED";
			}
			$selBody .= ">";
			$selBody .= $value[$valuePosition];
			$selBody .= "</option>";
		}
		return $selTop . $selBody . $selFooter;
	}

	public function zoneCatSelect() {
		echo $this->getSelect('selId', $this->getZoneCategories(), 5, 1);
	}
	
	public function getMaster($type, $typeId) {
		$bodyTop    = "<div class='form-area'><form role='form' id='frmDetail' name='frmDetail'>";
		$body       = '';
		$bodyBottom = "</form></div>";
		if ($type === 'Zone') { // ===== Get Master Lists =====
			$MasterList = $this->getZoneList();
		}
		elseif ($type === 'Style') {
			$MasterList = $this->getStyles();
		}
		elseif ($type === 'ZoneCategory') {
			$MasterList = $this->getZoneCategories();
		}
		if ($typeId !== 'z') { // ===== Detail =====
			if ($typeId === 'New') {
				$fieldNames = array_keys($MasterList[1]); //Array ( [0] => catId [1] => catDesc [2] => catCode ) 
				foreach ($fieldNames as $key => $field) {
					if (strpos($field, 'Id')) {
						$body .= "<input type='hidden' class='form-control getType' id='$type' value='$typeId'>";
					}
					else {
						$FieldHeader = $field;
						if ($this->PrettyHeaders[$field]) {
							$FieldHeader = $this->PrettyHeaders[$field];
						}
						$body .= "<div class='form-group'>";
						$body .= "<label for='$field'>$FieldHeader</label>";
						$body .= "<input type='text' class='form-control detail' id='$field' value='' placeholder='Enter new $FieldHeader'>";
					}
				}
			}
			else {
				$fieldNames = array_keys($MasterList[1]);
				foreach ($MasterList[$typeId] as $field => $value) {
					$body .= "<div class='form-group'>";
					if (strpos($field, 'Id')) {
						$body .= "<input type='hidden' class='form-control getType' id='$type' value='$typeId'>";
					}
					else {
						$FieldHeader = $field;
						if ($this->PrettyHeaders[$field]) {
							$FieldHeader = $this->PrettyHeaders[$field];
						}
//						$body .= "<div class='form-group'>";
						$body .= "<label for='$field'>$FieldHeader</label>";
						$body .= "<input type='text' class='form-control detail' oldVal='$value' id='$field' value='$value'>";
					}
					$body .= '</div>';
				}

			}
			$ret = $bodyTop . $body . $bodyBottom;
		}
		else {
			$ret = $MasterList;
		}
		return $ret;
	}
	
	public function masterList($type) {
		$MasterList = $this->getMaster($type, 'z');
		$Table = $this->buildTable($MasterList, $type);
		echo $Table;
	}
	
	private function buildTable($ArrayList, $sender) {
//		print_r($ArrayList);
//	[0] => Array ( [catId] => 1 [catDesc] => Living [catCode] => L )
//	[0] => Array ( [zoneId] => 1 [zoneDesc] => Living Room [zoneCategoryId] => 1 [Length] => 5.00 [Breadth] => 4.00 [isActive] => 1 [ViewBy] => 1
		$Headers = array_keys($ArrayList[1]);
		$TableHeader = "<table class='table table-hover table-responsive text-small master-list'><thead><tr class='filters'>";
		foreach ($Headers as $key => $field) {
			if (!is_numeric($field) && !strpos($field, 'Id') && $field !== 'isActive') {
				$header = $field;
				if (array_key_exists($field, $this->PrettyHeaders)) {
					$header = $this->PrettyHeaders[$field];
				}
				$TableHeader .= "<th><input type='text' class='form-control' placeholder='$header' disabled></th>";
			}
		}
		$TableHeader .= "</tr></thead>";
		$TableBody    = '<tbody>';
		foreach ($ArrayList as $Id => $data){
			$TableBody .= "<tr type='button' id='$Id' onclick='showDetail(\"$sender\", $Id)' >";
			foreach ($data as $field => $value) {
				if (!strpos($field, 'Id') && $field !== 'isActive') {
					if ($field === 'isActive') {
						if ($value == 1) {
							$value = "<i class='fa fa-check-square-o'></i>";
						}
						else {
							$value = "<i class='fa fa-window-close-o'></i>";
						}
					}
//					XXXX
					// TODO =====> change isActive display to tick fa-check-square-o or X fa-window-close-o
					$TableBody .= "<td>$value</td>";
				}
			}
			$TableBody .= "</tr>";
		}
		$TableBody .= "</tbody></table>";
		return $TableHeader . $TableBody;
	}
	
	public function updateMasters($type, $id, $data, $LoggedUser) {
//		echo "updateMasters($type, $id, $data, $LoggedUser)<br>";
		if ($data !== "delete") {
			$data = json_decode($data);
		}
		$fields = "";
		$values = "";
//		echo "classPlanDisplay<br>";
//		echo "Id: $id<br>";
//		echo "data/info: $data<br>";
		if ($type === "ZoneCategory") {$idField = "catId";}
		elseif ($type === "GeneralParam") {$idField = "ParamName";}
		else {$idField = strtolower($type . 'id');}
		if ($data === "delete") {
			$sql = "DELETE FROM `$type` WHERE `$idField` = $id";
		}
		else {
			if ($id === 'New') {
				foreach ($data as $field => $value) {
					$fields .= "`$field`, ";
					$values .= "'$value', ";
				}
				$fields .= "`CreateUser`, `CreateDate`, `ModUser`, `ModDate`";
				$values .= "$LoggedUser, '" . TODAY . "', $LoggedUser, '" . TODAY . "'";
				$sql = "INSERT `$type` ($fields) VALUES ($values)";
			}
			else {
				foreach ($data as $field => $value) {
					$values .= "`$field` =  '$value', ";
				}
//				$values = substr($values, 0, -2);
				$values .= " `ModUser` = $LoggedUser, `ModDate` = '" . TODAY . "'";
				$sql = "UPDATE `$type` SET " . $values . " WHERE `$idField` = '$id'";
//				echo $sql;
			}
		}
		$this->query($sql); 
	}
	
	protected function fetchPlanDetail($planId) {
		
	}
}
?>



