<!--==========================-->
<!--Wrapper for login security-->
<!--==========================-->
<?php
if (!$_SESSION) {session_start();}
if ($_SESSION['loggedIntoVAdminBackEnd'] === "UserHasSuccessfullyLoggedInToVAdminBackEnd" && $_SESSION['token'] === session_id()) {
	$LoggedUser = $_SESSION['LoggedUser'];
	include_once "config.php";
?>

<!-- Modal -->
<div id="modalDetail" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title" id="modal-title">Modal Detail</h4>
			</div>
			<!--<p class="btn-danger text-center" style="margin-bottom: 0px;">Changes made will be implemented immediately.</p>-->
			<div class="modal-body" id="modal-body">
				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-success" id="btnSave" onclick="saveMaster();">Save</button>
				<button type="button" class="btn btn-default btn-danger" id="btnDelete" onclick="deleteMaster()">Delete Record</button>
				<button type="button" class="btn btn-default" id="btnCancel" data-dismiss="modal">Cancel</button>
				<!--<button type="button" class="btn btn-default">Close</button>-->
			</div>
		</div>

	</div>
</div>
<?php 
	}
	else {
		session_destroy();
		header('location: index.php');
	}
?>


