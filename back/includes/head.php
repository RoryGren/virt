        <meta charset="UTF-8">
		<title>Virtual Architect Admin</title>

		<!-- Boostrap links -->
		<!--<link rel="stylesheet" type="text/css" media="all" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">-->
		<!--<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">-->
		<link href="style/bootstrap.min.css" rel="stylesheet" type="text/css"/>
		<link href="style/css/all.min.css" rel="stylesheet" type="text/css"/>

		<!-- Website CSS -->
		<link href="style/virtualArchitect.min.css" rel="stylesheet" type="text/css"/>
		<link href="style/va-Nav.min.css" rel="stylesheet" type="text/css"/>
		<!-- Website JS -->
		<!--<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>-->
		<script src="scripts/jquery-3.2.1.min.js" type="text/javascript"></script>
		<!--<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>-->
		<script src="scripts/bootstrap.min.js" type="text/javascript"></script>
		<script src="scripts/virtualArchitect.min.js" type="text/javascript"></script>
		<script src="scripts/va-core.min.js<?php echo "?d=" . date('c') ?>" type="text/javascript"></script>
