<?php
if ($_SERVER['SERVER_NAME'] === 'localhost') {
// ===================================================================
// ===== Comment these out for production. Debug Error Messages. =====
// ===================================================================
	ini_set('display_startup_errors', 1);
	ini_set('display_errors', 1);
	error_reporting(-1);
// ===================================================================
}
// ======================================================
// =====> Production Config constants and settings <=====
// ======================================================
date_default_timezone_set('Africa/Johannesburg');
define('TODAY',gmdate("Y-m-d H:i:s", strtotime(" + 2 hours")));
// ================================
// =====> PHP Root Constants <=====
// ================================
define('DIR_WEB_ROOT', dirname(__FILE__) . '/');	
define('DIR_ROOT', dirname(DIR_WEB_ROOT));
define('DIR_INC', DIR_ROOT . '/v-includes/');
define('DIR_CLASSES', DIR_WEB_ROOT . 'classes/');
?>
