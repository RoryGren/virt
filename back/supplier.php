<?php
session_start();
if ($_SESSION['loggedIntoVAdminBackEnd'] === "UserHasSuccessfullyLoggedInToVAdminBackEnd" && $_SESSION['token'] === session_id()) {
//		echo "<br><br><br><br>In Session!<br>";
	include_once "config.php";
?>
<!DOCTYPE html>
<html>
    <head>
		<?php include 'includes/head.php'; ?>
    </head>
    <body>
		<script type="text/javascript">
			$(document).ready(function() {
				setActive('menu-Supplier');
			});
		</script>
			<?php include 'includes/navBarTop.php'; ?>
			<div class="spacer"></div>
		<div class="container-fluid">
			<div class="row">
				<div class="col col-sm-2"></div>
				<div class="col col-sm-8">
					<h3 class="text-center">Virtual Architect Back-End</h3>
					<h4 class="text-center">Suppliers</h4>
					<p class="text-center">Menu options visible will differ, depending on user rights...</p>
				</div>
			</div>
		</div>
    </body>
</html>
<?php 
	}
	else {
		session_destroy();
		header('location: index.php');
	}
?>
