<?php
session_start();
if ($_SESSION['loggedIntoVAdminBackEnd'] === "UserHasSuccessfullyLoggedInToVAdminBackEnd" && $_SESSION['token'] === session_id()) {
	$LoggedUser = $_SESSION['LoggedUser'];
	include_once "config.php";
//	include_once WEB_ROOT . '/classes/classLogin.php';
?>
<!DOCTYPE html>
<html>
    <head>
		<?php include 'includes/head.php'; ?>
    </head>
    <body>
		<script type="text/javascript">
			$(document).ready(function() {
				sessionStorage.setItem('sender','Plans');
				setActive('menu-Plans');
				$('.leftNav>button').on('click', showFile);
			});
		</script>
			<?php include 'includes/navBarTop.php'; ?>
			<div class="spacer"></div>
		<div class="container-fluid">
			<div class="row">
				<div class="col col-sm-2 leftNav">
					<h3>Options</h3>
					<button id="planMasters"  class="btn btn-block btn-va" >Masters</button>
					<button id="planList"  class="btn btn-block btn-va" >Plans</button>
					<div id="planButtons" class="hidden">
						<hr>
						<p class="btn-block text-center"><u>Selected Plan</u>:<br>
							<span id="spanPlanCode"><i>Plan Code Here</i></span></p>
						<button id="planDetails" class="btn btn-block btn-va" title="Maintain details identifying selected plan.">Manage Details</button>
						<button id="planImages"  class="btn btn-block btn-va" title="Link Plan Images and Perspectives to the Plan.">Manage Images</button>
						<button id="planZones"   class="btn btn-block btn-va" title="Select areas on the selected plan to identify and link to Zones.">Manage Zones</button>
					</div>
				</div>
				<div class="col col-sm-8" id="working">
					<h3 class="text-center">Virtual Architect Back-End</h3>
					<p class="text-center">You are online...</p>
						<p class="text-center">Maintain & Upload plans - parameters, images, etc.</p>
					<p class="text-center">Menu options visible will differ, depending on user rights...</p>
				</div>
				<div class="col col-sm-2"></div>
			</div>
		</div>
		<?php 	include 'includes/modal.php'; ?>
    </body>
</html>
<?php 
	}
	else {
		session_destroy();
		header('location: index.php');
	}
?>
