
function setActive(buttonId) {
	var btnId = '#'+buttonId;
	$(btnId).siblings('button').removeClass('active');
	$(btnId).addClass('active');
}

function showFile(event) {
	var btnId  = event.target.id;
//	alert("Clicked: "+btnId);
	var sender = sessionStorage.getItem('sender');
	var path   = sender+'/'+btnId+'.php';
	if (btnId === 'planList') {
		$('#spanPlanCode').html('Select Plan from List');
	}
	setActive(btnId);
	$('#working').load(path);
}

function showDetail(type, typeId) {
	$('#modal-title').html('');
	$('#modal-body').html('');
	console.log("showDetail: "+type+", "+typeId);
	if (type === 'plan' && typeId !== 'New') {
		$('#'+typeId).siblings('tr').removeClass('active');
		$('#'+typeId).addClass('active');
	}
	if (typeId !== 'New') {
		console.log('Not New');
		$('#btnDelete').prop('disabled', '');
	}
	$.ajax({
		url:'Plans/showDetail.php',
		data: {
			type: type,
			typeId: typeId
		},
		success: function(data) {
			$('#modal-body').html(data);
			$('#modal-title').html($('#newTitle').val());
			$('#modalDetail').modal('show');
			$(':input').each(function() {
				$(this).data('initialValue', $(this).val());
			});
		}
	})
}

function saveMaster() {
	var masterType    = $('.getType').attr('id');
	var masterId      = $('.getType').val();
	var masterDiv     = '#div'+masterType;
	var changed       = false;
	var formData	  = {};
	var url2          = 'Plans/planLoadMaster.php';
	//oldVal attribute has old value for change comparison
	// Check all fields for changes
	$('.detail').each(function () {
		if ($(this).attr('oldVal') !== $(this).val()) {
			changed = true;
		}
		formData[this.id] = this.value;
	});
	if (masterId === 'New' || changed) {
		obj = JSON.stringify(formData);
		$.ajax({
			url:'Plans/planMasterUpdate.php',
			type: 'POST',
			data: {
				info:obj,
				type: masterType,
				id: masterId
			},
			success: function(data) {
				$('#modal-body').html(data); // ===== Switch these two lines for testing =====
//				$(masterDiv).html(data); ====> Need to $Plans->getMaster($type, $typeId)
			}
		}).then(
			$(masterDiv).load(url2, 'setType='+masterType)
		);
	}
	$('#modalDetail').modal('hide');
}

function saveParam(paramId) {
	var newParamVal = $('#'+paramId).val();
	var oldParamVal = $('#old_'+paramId).val();
	var obj = JSON.stringify({'ParamVal':newParamVal});
	$.ajax({
			url:'Plans/planMasterUpdate.php',
			type: 'POST',
			data: {
				info: obj,
				type: 'GeneralParam',
				id: paramId
			},
			success: function(data) {
//				$('#divCalcParams').html(data); // ===== Switch this off after testing =====
				// ===== Set Display =====
				$('#btn'+paramId).prepend("<i class='fa fa-check text-green'></i>");
				$('#btnC'+paramId).addClass('hide');
				$('#btnS'+paramId).addClass('hide');
			}
		});

}

function deleteMaster() {
	var masterType = $('.getType').attr('id');
	var masterId   = $('.getType').val();
	var masterDiv     = '#div'+masterType;
	var value      = $('.detail').first().attr('oldVal');
	var url2       = 'Plans/planLoadMaster.php';
	if (confirm('Delete '+value+'?')) {
		$.ajax({
			url:'Plans/planMasterUpdate.php',
			type: 'POST',
			data: {
				type: masterType,
				id: masterId,
				info: 'delete'
			},
			success: function(data) {
//				$(masterDiv).load(url2, 'setType='+masterType)
//				$('#modal-body').html(data);
//				alert(value+' is now gone!');
			}
		}).then(
			$(masterDiv).load(url2, 'setType='+masterType)
		);
	}
	$('#modalDetail').modal('hide');
}

function setActivePlan(planId) {
	sessionStorage.setItem('planId', planId);
	$('#planButtons').children('button').prop('disabled', false);
	$('#'+planId).siblings('tr').removeClass('active');
	$('#'+planId).addClass('active');
}

function showPlanDetail(sender) {
	showDetail(sender, sessionStorage.getItem('planId'));
}
