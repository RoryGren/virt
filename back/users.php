<?php
session_start();
if ($_SESSION['loggedIntoVAdminBackEnd'] === "UserHasSuccessfullyLoggedInToVAdminBackEnd" && $_SESSION['token'] === session_id()) {
	$LoggedUser = $_SESSION['LoggedUser'];
	include_once "config.php";
//	include_once WEB_ROOT . '/classes/classLogin.php';
?>
<!DOCTYPE html>
<html>
    <head>
		<?php include 'includes/head.php'; ?>
    </head>
    <body>
		<script type="text/javascript">
			$(document).ready(function() {
				setActive('menu-Users');
				$('.leftNav>button').on('click', showFile);
			});
		</script>
			<?php include 'includes/navBarTop.php'; ?>
			<div class="spacer"></div>
		<div class="container-fluid">
			<div class="row">
				<div class="col col-sm-2 leftNav">
					<h3>User Control</h3>
					<button id="userAccess" class="btn btn-block btn-va" >User Access</button>
					<button id="userDetail" class="btn btn-block btn-va" >User Detail</button>
				</div>
				<div class="col col-sm-8">
					<?php
						include 'User/userAccess.php';
					?>
				</div>
			</div>
		</div>
		<?php 	include 'includes/modal.php'; ?>
    </body>
</html>
<?php 
	}
	else {
		session_destroy();
		header('location: index.php');
	}
?>
