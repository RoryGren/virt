<?php
	session_start();
	if ($_SESSION['loggedIntoVAdminBackEnd'] === "UserHasSuccessfullyLoggedInToVAdminBackEnd" && $_SESSION['token'] === session_id()) {
//		echo "<br><br><br><br>In Session!<br>";
		$type = filter_input(INPUT_GET, setType);
		include_once '../config.php';
		include_once DIR_INC . "conn.inc.php";
		include_once DIR_WEB_ROOT . '/classes/classPlanDisplay.php';
		$Plans = new classPlanDisplay();
//		$Plans->getMaster($type, $typeId)
		$Plans->masterList($type);
?>
<?php 
	}
	else {
		session_destroy();
		header('location: index.php');
	}
?>
