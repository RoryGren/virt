<?php
	session_start();
	if ($_SESSION['loggedIntoVAdminBackEnd'] === "UserHasSuccessfullyLoggedInToVAdminBackEnd" && $_SESSION['token'] === session_id()) {
//		echo "<br><br><br><br>In Session!<br>";
//	echo "<br>planMasterUpdate.php<br>";
	$type = filter_input(INPUT_POST, type); // Type ==> Style / Zone / ZoneCat
	$id   = filter_input(INPUT_POST, id); // Type ==> Style / Zone / ZoneCat
	$data = filter_input(INPUT_POST, info); // Form field data
	$LoggedUser = $_SESSION['LoggedUser'];
//	echo "planMasterUpdate.php<br>$type, $id, $data, $LoggedUser<br>";
	include_once '../config.php';
	include_once DIR_INC . "conn.inc.php";
	include_once DIR_WEB_ROOT . '/classes/classPlanDisplay.php';
	$master = new classPlanDisplay();
//	echo "info/data: $data<br>";
	$master->updateMasters($type, $id, $data, $LoggedUser);
//	print_r($Data);
//	$info = json_decode($info);
//	echo "<br>Type: $type<br>";
//	echo "Id: $id<br>info:<br>==========================<br>";
//	foreach ($info as $key => $value) {
//		echo "$key = $value<br>";
//	}
	}
	else {
		session_destroy();
		header('location: index.php');
	}
?>
