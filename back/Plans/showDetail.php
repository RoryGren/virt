<?php
session_start();
if ($_SESSION['loggedIntoVAdminBackEnd'] === "UserHasSuccessfullyLoggedInToVAdminBackEnd" && $_SESSION['token'] === session_id()) {
//	echo "<br><br><br><br>In Session!<br>";
	include_once '../config.php';
	include_once DIR_INC . "conn.inc.php";
	$type   = filter_input(INPUT_GET,'type');
	$typeId = filter_input(INPUT_GET,'typeId');
	include_once DIR_WEB_ROOT . '/classes/classPlanDisplay.php';
	$Plans = new classPlanDisplay();
//	echo "Type: " . substr($type, 0, 4) . "<br>";
	if (substr($type, 0, 4) === 'plan') { // Plans
		echo "Id selected: $typeId<br>";
	}
	else { // Masters...
//		echo "MasterList: " . $type . "<br>";
		echo $Plans->getMaster($type, $typeId);
		// TODO =====> get fields
		// TODO =====> present fields
		// TODO =====> put values into fields
	}
//	echo "Type: $type<br>Selected Id: $typeId<br>";
//	echo "List<br>";
//	print_r($List);
//	$Plans->displayDetails($List);
?>
<input type="hidden" id="newTitle" value="<?php echo $type; ?>">
<?php 
	}
	else {
		session_destroy();
		header('location: index.php');
	}
?>
