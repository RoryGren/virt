<?php
if (!$_SESSION){session_start();}
if ($_SESSION['loggedIntoVAdminBackEnd'] === "UserHasSuccessfullyLoggedInToVAdminBackEnd" && $_SESSION['token'] === session_id()) {
?>
<header class="navbar navbar-light navbar-fixed-top bs-docs-nav va-orange" role="banner">
	<a class="navbar-brand" href="index.php"><p class="logo"><span class="textDark">Virtual</span><span class=textWhite>Architect</span></p></a>
	<div class="container">
		<div class="navbar-header">
			<button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
		<nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation" id="navbarNav">
			<ul class="nav navbar-nav navbar-left">
				<li id="menu-Home" class="active">
					<a href="index.php" class="transition">Home<span class="sr-only">(current)</span></a>
				</li>
				<li id="menu-Plans">
					<a href="plans.php" title="Maintain Plans" class="transition">Plans</a>
				</li>
				<li id="menu-Services">
					<a href="services.php" title="Maintain Other Services" class="transition">Services</a>
				</li>
				<li id="menu-Customer">
					<a href="customer.php" title="Maintain Clients" class="transition">Clients</a>
				</li>
				<li id="menu-Projects" title="Maintain Projects">
					<a href="projects.php" title="Maintain Projects">Projects</a>
				</li>
				<li id="menu-Contract">
					<a href="contact.php" title="Maintain Contractors" class="transition">Contractors</a>
				</li>
				<li id="menu-Suppliers">
					<a href="supplier.php" title="Maintain Suppliers" class="transition">Suppliers</a>
				</li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li id="menu-Login">
					<a href="#logout" class="transition"><span class="fa fa-sign-in" style="font-size: large;"></span> Logout</a>
				</li>
			</ul>
		</nav>
	</div>
</header>
<?php 
	}
	else {
		session_destroy();
		header('location: index.php');
	}
?>
