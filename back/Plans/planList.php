<?php
	session_start();
	if ($_SESSION['loggedIntoVAdminBackEnd'] === "UserHasSuccessfullyLoggedInToVAdminBackEnd" && $_SESSION['token'] === session_id()) {
//		echo "<br><br><br><br>In Session!<br>";
		include_once '../config.php';
		include_once DIR_INC . "conn.inc.php";
		include_once DIR_WEB_ROOT . '/classes/classPlanDisplay.php';
		$Plans = new classPlanDisplay();
?>
	<script type="text/javascript">
		$(document).ready(function() {
			$("table tbody tr").on('click', function(event) {
				var planId  = this.id;
				var planCode = $(this).closest('tr').find('td:first-child').html();
				$('#spanPlanCode').html('<i>'+planCode+'</i>');
				setActivePlan(planId);
//				showDetail('plan', planId);
			});
			
			$('#planButtons').children('button').on('click', function() {
//				alert(this.id);
				showPlanDetail(this.id);
			});

			$('.filterable .btn-filter').click(function(){
				var $panel = $(this).parents('.filterable'),
				$filters = $panel.find('.filters input'),
				$tbody = $panel.find('.table tbody');
				if ($filters.prop('disabled') == true) {
					$filters.prop('disabled', false);
					$filters.first().focus();
				} else {
					$filters.val('').prop('disabled', true);
					$tbody.find('.no-result').remove();
					$tbody.find('tr').show();
				}
			});

			$('.filterable .filters input').keyup(function(e){
				/* Ignore tab key */
				var code = e.keyCode || e.which;
				if (code == '9') return;
				/* Useful DOM data and selectors */
				var $input = $(this),
				inputContent = $input.val().toLowerCase(),
				$panel = $input.parents('.filterable'),
				column = $panel.find('.filters th').index($input.parents('th')),
				$table = $panel.find('.table'),
				$rows = $table.find('tbody tr');
				/* Dirtiest filter function ever ;) */
				var $filteredRows = $rows.filter(function(){
					var value = $(this).find('td').eq(column).text().toLowerCase();
					return value.indexOf(inputContent) === -1;
				});
				/* Clean previous no-result if exist */
				$table.find('tbody .no-result').remove();
				/* Show all rows, hide filtered ones (never do that outside of a demo ! xD) */
				$rows.show();
				$filteredRows.hide();
				$('#RowCount').html("   ("+($rows.length-$filteredRows.length)+" rows)");
				/* Prepend no-result row if all rows are filtered */
				if ($filteredRows.length === $rows.length) {
					$table.find('tbody').prepend($('<tr class="no-result text-center"><td colspan="'+ $table.find('.filters th').length +'">No result found</td></tr>'));
				}

			});
			$('#planButtons').removeClass('hidden');
			$('#planButtons button').prop('disabled', true);
		});
	</script>
	<div class="panel panel-va filterable">
		<div class="panel-heading">
			<h4>
				<button class="btn btn-default btn-xs btn-filter"><span class="fas fa-filter"></span> Filter</button>&nbsp;&nbsp;&nbsp;
				Uploaded Plans&nbsp;&nbsp;&nbsp;
				<button class="btn btn-danger btn-xs" onclick="showDetail('plan', 'New')">Add</button>
			</h4>
			<div>
			</div>

		</div>

		<div id="PlanList" class="container-fluid">
		<?php 	$Plans->showPlanList(); ?>
		</div>
	</div>		
<?php 
	}
	else {
		session_destroy();
		header('location: index.php');
	}
?>
