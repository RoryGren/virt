<?php
	session_start();
	if ($_SESSION['loggedIntoVAdminBackEnd'] === "UserHasSuccessfullyLoggedInToVAdminBackEnd" && $_SESSION['token'] === session_id()) {
//		echo "<br><br><br><br>In Session!<br>";
		include_once '../config.php';
		include_once DIR_INC . "conn.inc.php";
		include_once DIR_WEB_ROOT . '/classes/classPlanDisplay.php';
		$Plans = new classPlanDisplay();
	//	$Plans->zoneCatSelect();
?>
	<script type="text/javascript">
		$(document).ready(function() {
//			$("table tbody tr").on('click', function() {
//				var sender = $('div.active').attr('id');
//				var rowId  = this.id;
//				showDetail(sender, rowId);
//			})
			$('.filterable .btn-filter').click(function(){
				var $panel = $(this).parents('.filterable'),
				$filters = $panel.find('.filters input'),
				$tbody = $panel.find('.table tbody');
				if ($filters.prop('disabled') == true) {
					$filters.prop('disabled', false);
					$filters.first().focus();
				} else {
					$filters.val('').prop('disabled', true);
					$tbody.find('.no-result').remove();
					$tbody.find('tr').show();
				}
			});

			$('.filterable .filters input').keyup(function(e){
				/* Ignore tab key */
				var code = e.keyCode || e.which;
				if (code == '9') return;
				/* Useful DOM data and selectors */
				var $input = $(this),
				inputContent = $input.val().toLowerCase(),
				$panel = $input.parents('.filterable'),
				column = $panel.find('.filters th').index($input.parents('th')),
				$table = $panel.find('.table'),
				$rows = $table.find('tbody tr');
				/* Dirtiest filter function ever ;) */
				var $filteredRows = $rows.filter(function(){
					var value = $(this).find('td').eq(column).text().toLowerCase();
					return value.indexOf(inputContent) === -1;
				});
				/* Clean previous no-result if exist */
				$table.find('tbody .no-result').remove();
				/* Show all rows, hide filtered ones (never do that outside of a demo ! xD) */
				$rows.show();
				$filteredRows.hide();
				$('#RowCount').html("   ("+($rows.length-$filteredRows.length)+" rows)");
				/* Prepend no-result row if all rows are filtered */
				if ($filteredRows.length === $rows.length) {
					$table.find('tbody').prepend($('<tr class="no-result text-center"><td colspan="'+ $table.find('.filters th').length +'">No result found</td></tr>'));
				}

			});
			$('#planButtons').addClass('hidden');
			$('.saveInputs').children('input:text').on('keyup', function() {
				var inputId = this.id;
				var newVal  = $('#'+inputId).val();
				var oldVal  = $('#old_'+inputId).val();
				if (newVal !== oldVal) { // Show buttons
					console.log('changed!');
//					$('.saveButtons').children('button').removeClass('hidden');
					$('#btnS'+inputId).removeClass('hide');
					$('#btnC'+inputId).removeClass('hide');
				}
				else { // Hide buttons
					if ($('#btnC'+inputId).not('.hide')) {
						$('#btnS'+inputId).addClass('hide');
						$('#btnC'+inputId).addClass('hide');
					}
				}
			});
		});
		function cancelParam(paramId) {
			$('#'+paramId).val($('#old_'+paramId).val());
			$('#btnS'+paramId).addClass('hide');
			$('#btnC'+paramId).addClass('hide');
		}
	</script>

<div class="masters">
	<h4 title="Maintain Master Lists" class="pointer">Masters</h4>
	<ul class="nav nav-tabs">
		<li class="active">	<a data-toggle="tab" href="#Style" >Styles</a></li>
		<li>				<a data-toggle="tab" href="#ZoneCategory">Zone Categories</a></li>
		<li>				<a data-toggle="tab" href="#Zone"  >Zones</a></li>
		<li>				<a data-toggle="tab" href="#CalcParams"  >Calculation Parameters</a></li>
	</ul>

	<div class="tab-content"> <!-- ----- Tab Content ------->
<!-- Styles -->
		<div id="Style" class="tab-pane fade in active">
			<div class="panel panel-dark filterable">
				<div class="panel-heading">
					<div>
						<h4>
							<button class="btn btn-default btn-xs btn-filter"><span class="fas fa-filter"></span> Filter</button>&nbsp;&nbsp;&nbsp;
							Styles&nbsp;&nbsp;&nbsp;
							<button class="btn btn-danger btn-xs" onclick="showDetail('Style', 'New')">Add</button>
						</h4>
					</div>

				</div>
				<div id="divStyle">
					<?php $Plans->masterList('Style'); ?>
				</div>
			</div>
	   </div>
<!-- Zone Categories -->
		<div id="ZoneCategory" class="tab-pane fade">
			<div class="panel panel-dark filterable">
				<div class="panel-heading">
					<div>
						<h4>
							<button class="btn btn-default btn-xs btn-filter"><span class="fas fa-filter"></span> Filter</button>&nbsp;&nbsp;&nbsp;
							Zone Categories&nbsp;&nbsp;&nbsp;
							<button class="btn btn-danger btn-xs" onclick="showDetail('ZoneCategory', 'New')">Add</button>
						</h4>
					</div>
				</div>
				<div id="divZoneCategory">
					<?php $Plans->masterList('ZoneCategory'); ?>
				</div>
			</div>
		</div>
<!-- Zones -->
		<div id="Zone" class="tab-pane fade">
			<div class="panel panel-dark filterable">
				<div class="panel-heading">
					<div>
						<h4>
							<button class="btn btn-default btn-xs btn-filter"><span class="fas fa-filter"></span> Filter</button>&nbsp;&nbsp;&nbsp;
							Zones&nbsp;&nbsp;&nbsp;
							<button class="btn btn-danger btn-xs" onclick="showDetail('Zone', 'New')">Add</button>
						</h4>
					</div>
				</div>
				<div id="divZone">
					<?php $Plans->masterList('Zone'); ?>
				</div>
			</div>
		</div> 
<!-- CalcParams -->
		<div id="CalcParams" class="tab-pane fade">
			<div class="panel panel-dark filterable">
				<div class="panel-heading">
					<div>
						<h4>
							<!--<button class="btn btn-default btn-xs btn-filter"><i class="fa fa-filter"></i> Filter</button>&nbsp;&nbsp;&nbsp;-->
							Calculation Parameters&nbsp;&nbsp;&nbsp;
							<!--<button class="btn btn-danger btn-xs" onclick="showDetail('Zone', 'New')">Add</button>-->
						</h4>
					</div>
				</div>
				<div id="divCalcParams">
					<?php $Plans->showGeneralParams(); ?>
				</div>
			</div>
		</div> 
<!-- ----- End Tab Content ------->
	</div>
</div>
<?php 
	}
	else {
		session_destroy();
		header('location: index.php');
	}
?>
