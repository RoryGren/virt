<?php
	session_start();
//	Array ( [userName] => RoryGren [pwd] => 620325 [submit] => doLogin ) 
	if (isset($_POST['userName']) && isset($_POST['pwd']) && isset($_POST['submit'])) {
		$userName = filter_input(INPUT_POST, userName);
		$password = filter_input(INPUT_POST, pwd);
		include_once "config.php";
		include_once DIR_CLASSES . "classLogin.php";
		$login = new classLogin($userName, $password);
		if ($login->checkValidLogin()) {
			$_SESSION['loggedIntoVAdminBackEnd'] = "UserHasSuccessfullyLoggedInToVAdminBackEnd";
			$_SESSION['token'] = session_id();
			$_SESSION['LoggedUser'] = $login->getLoggedUser();
			header('location: online.php');
		}
	}
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
		<meta name="theme-color" content="#ff9500"/>
		<title>Virtual Architect | Admin Login</title>

		<!-- Boostrap links -->
		<link rel="stylesheet" type="text/css" media="all" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="all" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

		<!-- Website CSS -->
		<link href="style/virtualArchitect.min.css" rel="stylesheet" type="text/css"/>
		<!-- Website JS -->
		<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script src="scripts/virtualArchitect.min.js" type="text/javascript"></script>
		<script src="scripts/va-core.min.js" type="text/javascript"></script>
    </head>
    <body>
		<div class="container-fluid">
			<div class="row padded-top">
				<div class="col col-sm-4"></div>
				<div class="col col-sm-4">
					<form role="form" id="login_form" name="login_form" method="post">
						<div class="form-area">  
							<div class="form-group">
								<label for="userName">Username</label>
								<input type="text" class="form-control" id="userName" name="userName" placeholder="userName" required >
							</div>
							<div class="form-group">
								<label for="pwd">Password</label>
								<input type="password" class="form-control" id="pwd" name="pwd" required >
							</div>
							<div class="form-group padded-top text-center">
								<button type="submit" class="btn btn-danger" id="submit" name="submit" value="doLogin">Login</button>
							</div>
						</div>
					</form>
				</div>
				<div class="col col-sm-4"></div>
			</div>
		</div>
    </body>
</html>
<?php
//	}
?>