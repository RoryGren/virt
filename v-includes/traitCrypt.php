<?php
	trait traitCrypt {

		private $cipher_algo = 'aes-256-ctr';
		private $hash_algo = 'sha256';
		private $iv_num_bytes;
		private $iv;
		private $tag;

		/**
		 * =========================================================================
		 * Encrypt a string.
		 * @param  string $in  String to encrypt.
		 * @param  string $key Encryption key.
		 * @param  int $fmt Optional override for the output encoding. One of FORMAT_RAW, FORMAT_B64 or FORMAT_HEX.
		 * @return string      The encrypted string.
		 */
		public function encryptString($in, $key) {
			// ==============================
			// Build an initialisation vector
			// ==============================
			$this->iv_num_bytes = openssl_cipher_iv_length($this->cipher_algo);
			$iv = openssl_random_pseudo_bytes($this->iv_num_bytes, $isStrongCrypto);
			if (!$isStrongCrypto) {
				throw new \Exception("Cryptor::encryptString() - Not a strong key");
			}
			// ============
			// Hash the key
			// ============
			$keyhash = openssl_digest($key, $this->hash_algo, true);
			// ============
			// and encrypt
			// ============
			$opts =  OPENSSL_RAW_DATA;
			$encrypted = openssl_encrypt($in, $this->cipher_algo, $keyhash, $opts, $iv);
			if ($encrypted === false) {
				throw new \Exception('Cryptor::encryptString() - Encryption failed: ' . openssl_error_string());
			}
			// =====================================================================
			// The result comprises the IV and encrypted data formatted in base64
			// =====================================================================
			$res = base64_encode($iv . $encrypted);

			return $res;
		}

		/**
		 * =========================================================================
		 * Decrypt a string.
		 * @param  string $in  String to decrypt.
		 * @param  string $key Decryption key.
		 * @param  int $fmt Optional override for the input encoding. One of FORMAT_RAW, FORMAT_B64 or FORMAT_HEX.
		 * @return string      The decrypted string.
		 */
		public function decryptString($in, $key) {
			$raw = $in;
			// ==========================
			// Restore the encrypted data
			// ==========================
			$raw = base64_decode($in);
			// =====================================
			// and do an integrity check on the size.
			// =====================================
			if (strlen($raw) < $this->iv_num_bytes) {
				throw new \Exception('Cryptor::decryptString() - ' .
					'data length ' . strlen($raw) . " is less than iv length {$this->iv_num_bytes}");
			}
			// ====================================================
			// Extract the initialisation vector and encrypted data
			// ====================================================
			$iv = substr($raw, 0, $this->iv_num_bytes);
			$raw = substr($raw, $this->iv_num_bytes);
			// ============
			// Hash the key
			// ============
			$keyhash = openssl_digest($key, $this->hash_algo, true);
			// ============
			// and decrypt.
			// ============
			$opts = OPENSSL_RAW_DATA;
			$res = openssl_decrypt($raw, $this->cipher_algo, $keyhash, $opts, $iv);
			if ($res === false) {
				throw new \Exception('Cryptor::decryptString - decryption failed: ' . openssl_error_string());
			}
			return $res;
		}
	}
?>
