<?php
include DBCONN;
/**
 * Description of classSearch
 *
 * @author rory
 */
class classSearch extends classDB {

	private $Searchable;
	private $PlanMedia;
	private $Plans;

	public function __construct() {
		parent::__construct();
		$this->fetchSearchables();
	}
	
	public function getSearchables() {
		return $this->Searchable;
	}

	public function getPlans($searchParams) {
		$list = $this->fetchPlans($searchParams);
		return $list;
	}
	
	public function getPlanList() {
		return $this->Plans;
	}

	private function fetchSearchables() {
		$SQL = 'SELECT `searchableId`, `SearchableDesc`, `SearchableFieldName`, `ViewBy`
				FROM `Searchables`
				ORDER BY `ViewBy`';
		$query = $this->getData($SQL);
		while ($row = mysqli_fetch_assoc($query)) {
			$this->Searchable[] = $row;
		}
	}

	private function fetchPlans($searchParams) {
		$SQL = "SELECT * FROM `Plan` WHERE ";
		foreach ($searchParams as $key => $value) {
			if ($value === 'true') {$value = 1;}
			elseif ($value === 'false') {$value = 0;}
			if ($value > 0) {
				if (substr($SQL, -6) != 'WHERE ') {
					$SQL .= " AND ";
				}
				$SQL .= "`$key` = '$value' ";
			}
		}
		$query = $this->getData($SQL);
		while ($row = mysqli_fetch_assoc($query)) {
			$this->Plans[$row['PlanId']] = $row;
			$this->PlanMedia[] = $row['PlanId'];
		}
		return $this->PlanMedia;
	}
	
	protected function fetchPlanImages($planId) {
		$SQL = "SELECT * FROM `Plan` WHERE `PlanId` = $planId";
		$QRY = $this->query($SQL);
		return $QRY;
	}
	
	protected function getStyleList() {
		$result = '';
		$SQL = "SELECT `StyleId`, `StyleDesc` FROM `Style`";
		$QRY = $this->query($SQL);
		return $QRY;
	}
}

?>
