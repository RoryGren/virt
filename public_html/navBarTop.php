<header class="navbar navbar-light navbar-fixed-top bs-docs-nav bg-light" role="banner">
	<a class="navbar-brand" href="index.php"><p class="logo"><span class="textDark">Virtual</span><span class=textWhite>Architect</span></p></a>
	<div class="container">
		<div class="navbar-header">
			<button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".bs-navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
		<nav class="collapse navbar-collapse bs-navbar-collapse" role="navigation" id="navbarNav">
			<ul class="nav navbar-nav navbar-left">
				<li id="menu-Home" class="active">
					<a href="index.php" class="transition">Home<span class="sr-only">(current)</span></a>
				</li>
				<li id="menu-About">
					<a href="aboutUs.php" title="A little bit about us" class="transition">About Us</a>
				</li>
				<li id="menu-Services">
					<a href="services.php" title="What we can do for you" class="transition">Services</a>
				</li>
				<li id="menu-Search" title="Design Your Own">
					<a href="search.php" class="transition">Design Your Own</a>
				</li>
				<li id="menu-Contact">
					<a href="contact.php" title="Is this too confusing?" class="transition">Contact us</a>
				</li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
				<li id="menu-Login">
					<a href="#login" onclick="alert('Sorry! Not yet!')" class="transition"><span class="fa fa-sign-in" style="font-size: large;"></span> Login</a>
				</li>
			</ul>
		</nav>
	</div>
</header>
