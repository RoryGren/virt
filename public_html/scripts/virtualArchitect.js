//Adding smooth scrolling =====> Canned scrolling. Separated pages.
//$(document).ready(function(){
//    $("a").on('click', function(event) {
//        if (this.hash !== "") {
//          event.preventDefault();
//    
//          var hash = this.hash;
//    
//          $('html, body').animate({
//            scrollTop: $(hash).offset().top
//          }, 800, function(){
//      
//            window.location.hash = hash;
//          });
//        }
//    });
//});
//
////Navbar changing active tab =====> Put into Core.js
//(function ($) {
//    'use strict';
//    var results = {};
//
//    function display() {
//        var resultString = '';
//
//        $.each(results, function (key) {
//            resultString += '(' + key + ': ' + Math.round(results[key]) + '%)';
//        });
//
//        //$('p').text(resultString);
////        console.log(resultString);
//    }
//
//    function calculateVisibilityForDiv(div$) {
//        var windowHeight = $(window).height(),
//            docScroll = $(document).scrollTop(),
//            divPosition = div$.offset().top,
//            divHeight = div$.height(),
//            hiddenBefore = docScroll - divPosition,
//            hiddenAfter = (divPosition + divHeight) - (docScroll + windowHeight);
//
//            if ((docScroll > divPosition + divHeight) || (divPosition > docScroll + windowHeight)) {
//                return 0;
//            } 
//            else {
//                var result = 100;
//
//                if (hiddenBefore > 0) {
//                    result -= (hiddenBefore * 100) / divHeight;
//                }
//
//                if (hiddenAfter > 0) {
//                    result -= (hiddenAfter * 100) / divHeight;
//                }
//
//                return result;
//            }
//    }
//
//    function calculateAndDisplayForAllDivs() {
//        $('div').each(function () {
//            var div$ = $(this);
//            results[div$.attr('id')] = calculateVisibilityForDiv(div$);
//        });
//
//        display();
//    }
//
//    $(document).scroll(function () {
//        calculateAndDisplayForAllDivs();
//    });
//
//    $(document).ready(function () {
//        calculateAndDisplayForAllDivs();
//    });
//}(jQuery));

function doSearch() {
	$('.planSearch').empty();
	$('#planSearch').css('background-color', '');
//	alert('doSearch');
//	$('#frmSearch input').each(
//		function(index){  
//			var input = $(this);
//			alert('Type: ' + input.attr('type') + 'Name: ' + input.attr('name') + 'Value: ' + input.val());
//		}
//	);
	var Bedrooms     = $('#Bedrooms').val();
	var Bathrooms    = $('#Bathrooms').val();
	var Garages      = $('#Garages').val();
	
	var LivingRoom   = $('#LivingRoom').prop('checked');
	var FamilyRoom   = $('#FamilyRoom').prop('checked');
	var DiningRoom   = $('#DiningRoom').prop('checked');
    var Study        = $('#Study').prop('checked');
    var EntranceHall = $('#EntranceHall').prop('checked');
    var SewingRoom   = $('#sewRoom').prop('checked');
    var Laundry      = $('#Laundry').prop('checked');
    var Scullery     = $('#Scullery').prop('checked');
    var Pantry       = $('#Pantry').prop('checked');
    var WineCellar   = $('#WineCellar').prop('checked');
    var Workshop     = $('#Workshop').prop('checked');
	var SQ           = $('#SQ').prop('checked');
    var GF           = $('#GF').prop('checked');
    var Pool         = $('#Pool').prop('checked');
	var KitchenCourtyard = $('#KitchenCourtyard').prop('checked');
    var StoreRoom    = $('#StoreRoom').prop('checked');
    var Carport      = $('#Carport ').prop('checked');

	$.ajax({
		url: 'searchFirst.php',
		data:{
			Bedrooms:   Bedrooms,
			Bathrooms:  Bathrooms,
			Garages:    Garages,
			
			LivingRoom: LivingRoom,
			FamilyRoom: FamilyRoom,
			DiningRoom: DiningRoom,
            Study: Study,
            EntranceHall: EntranceHall,
            SewingRoom: SewingRoom,
            Laundry: Laundry,
            Scullery: Scullery,
            Pantry: Pantry,
            WineCellar: WineCellar,
            Workshop: Workshop,
			SQ: SQ,
            GF: GF,
            Pool: Pool,
			KitchenCourtyard: KitchenCourtyard,
            StoreRoom: StoreRoom,
            Carport: Carport
        },
		success: function(data){
			$('#searchResults').html(data);
		}
	});
}

function showPlan(planId) {
	$.ajax({
		url:'showPlans.php',
		data:{planId:planId},
		success: function(data) {
			$('#planSearch').css('background-color', '#FFF');
			$('#planSearch').html(data);
			$('#planCode').text($('#plan'+planId).val());
			$('#planCodeId').val($('#plan'+planId).val());
		}
	});
}

function swapImages(imageName) {
	$('.showBig').attr('src',imageName);
}

var added = [];
function addItem(description, id) {
    var alreadyAdded = added.indexOf(id);

    if(alreadyAdded == -1) {
        added.push(id);
        var check = document.createElement('tr');

        check.className = 'form-group form-inline';

        check.innerHTML = '<td><label for="' + id + '" class="search">' + description + '</label></td>\
                        <td><input type="checkbox" class="form-control" id="' + id + '" name="' + id + '" onchange="doSearch();"></td>'
        
        document.getElementById('tblSelection').appendChild(check);
		doSearch();
    }
}


