<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
		<meta name="theme-color" content="#ff9500"/>
		<title>Virtual Architect</title>

		<!-- Boostrap links -->
		<link rel="stylesheet" type="text/css" media="all" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="all" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

		<!-- Website CSS -->
		<link href="style/virtualArchitect.min.css" rel="stylesheet" type="text/css"/>
		<!-- Website JS -->
		<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script src="scripts/virtualArchitect.min.js" type="text/javascript"></script>
		<script src="scripts/va-core.min.js" type="text/javascript"></script>
		<script type="text/javascript">$("body").css("display", "none");</script>
    </head>
    <body class="pages" style="display: none;">
		<script type="text/javascript">
			$(document).ready(function() {
				doTransition();
			});
		</script>
		<?php include 'navBarTop.php'; ?>
		<!-- Home page -->
		<div class="home pages" id="home"><a id="home"></a>
			<div class="container" style="margin-top: 70px;">
				<!--<div class="row">-->
					<?php  include 'carouselHome.php'; ?>
					<p class="text-center">Welcome...</p>
				<!--</div>-->
			</div>
		</div>
    </body>
</html>
