<?php
//if ($_SERVER['SERVER_NAME'] === 'localhost') {
// ===================================================================
// ===== Comment these out for production. Debug Error Messages. =====
// ===================================================================
	ini_set('display_startup_errors', 1);
	ini_set('display_errors', 1);
	error_reporting(-1);
// ===================================================================
//}
// ======================================================
// =====> Production Config constants and settings <=====
// ======================================================
date_default_timezone_set('Africa/Johannesburg');

// ================================
// =====> PHP Root Constants <=====
// ================================

define('WEB_ROOT', dirname(__FILE__) . '/');	// /Users/rory/Sites/ii-eLearning/public_html/
define('BASE', dirname(WEB_ROOT));				// /Users/rory/Sites/ii-eLearning
define('DOM', $_SERVER['SERVER_NAME']);			// localhost

// ========================================
/* File containing DB connection details */
// ========================================

//echo "BASE: " . BASE . "<br>";
$Pos = strrpos(BASE, '/');
//echo $Pos . "<br>";
$Base = substr(BASE, 0, $Pos);
$Conn = '/virtual_includes/conn.inc.php';
if ($_SERVER['SERVER_NAME'] === 'localhost') {
	define('INC', BASE . $Conn);
	define('DBCONN', BASE.'/virtual_includes/classDB.php');
}
else {
	define('INC', BASE . $Conn);
	define('DBCONN', BASE.'/virtual_includes/classDB.php');
}
//	echo "<br>INC: " . INC . "<br>";
//	echo "<br>DBCONN: " . DBCONN . "<br>";

// =================================
// =====> HTML Root Constants <=====
// =================================

?>
