<?php
	include 'config.php';
	include 'classes/classSearch.php';
	$Search = new classSearch();
	$SearchableList = $Search->getSearchables();
	$SearchableCounter = 0;
//	echo "<br><br><br><br>";
//	print_r($SearchableList);
//	foreach ($SearchableList as $key => $data) {
//		echo "$key:: ";
//		print_r($data);
//		echo "<br>";
//	}
?>
<!DOCTYPE html>
<html>
	<head>
        <meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="theme-color" content="#ff9500"/>
		<title>Virtual Architect</title>

		<!-- Boostrap links -->
		<link rel="stylesheet" type="text/css" media="all" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" media="all" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

		<!-- Website CSS -->
		<link href="style/virtualArchitect.min.css?d=<?php echo date('YmdHis'); ?>" rel="stylesheet" type="text/css"/>
		<!-- Website JS -->
		<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		<script src="scripts/virtualArchitect.min.js?d=<?php echo date('YmdHis'); ?>" type="text/javascript"></script>
		<script src="scripts/va-core.min.js" type="text/javascript"></script>
		<script type="text/javascript">$("body").css("display", "none");</script>
	</head>
	<body class="pages" style="display: none;">
		<script type="text/javascript">
			$(document).ready(function() {
				doTransition();
				setActive('#menu-Search');
			});
			$("body").on("click", "#searchResults", function(e) {
				if (e.target.localName === "div") {
					showPlan(e.target.id);
				}
			});
		</script>
		<?php include 'navBarTop.php'; ?>
		
		<div class="container-fluid">
			<div class="row">
				<div class="col col-md-2 search"></div>
				<div class="col col-md-10">
					<h3 class="text-center">Design Your Dream Home</h3>
				</div>
			</div>
			<div class="row">
				<div class="col col-sm-1"></div>
				<div class="col col-sm-2 search">
					<h4 class="search">Step 1: Find the floor-plan!</h4>
					<form id="frmSearch" class="search">

						<div class="form-group">
							<label for="Bedrooms" class="search">How many Bedrooms would you like?</label>
							<input type="number" class="form-control" id="Bedrooms" name="Bedrooms" onchange="doSearch();" autofocus>
						</div>

						<div class="form-group">
							<label for="Bathrooms" class="search">How many Bathrooms would you like?</label>
							<input type="number" class="form-control" id="Bathrooms" name="Bathrooms" placeholder="1, 1.5, 2, 2.5, ..." onchange="doSearch();">
						</div>
						<div class="form-group">
							<label for="Garages" class="search">How many Garages would you like?</label>
							<input type="number" class="form-control" id="Garages" name="Garages" value="0" onchange="doSearch();">
						</div>
						<div class="form-group">
							<table class="table table-bordered">
								<tbody  id="tblSelection">
								<?php
									while ($SearchableCounter < 5) {
										if (isset($SearchableList[$SearchableCounter])){
											echo "<tr class='form-group form-inline'>";
											echo "<td><label for='" . $SearchableList[$SearchableCounter]['SearchableFieldName'] . "' class='search'>"
													. $SearchableList[$SearchableCounter]['SearchableDesc'] . " </label></td>";
											echo "<td><input type='checkbox' class='form-control' id='" . $SearchableList[$SearchableCounter]['SearchableFieldName']
													. "' name='" . $SearchableList[$SearchableCounter]['SearchableFieldName'] . "' onchange='doSearch();'></td>";
											echo "</tr>";
											$SearchableCounter ++;
										}
									}
								?>
									<div id="extraRooms">

									</div>
								</tbody>
							</table>
						</div>
						<div class="btn-group dropup">
							<button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								Other Rooms
							</button>
							<ul class="dropdown-menu" aria-labelledby="dropdownMenuButton">
								<?php
								foreach ($SearchableList as $key => $value) {
									if ($key > 4) {
										echo "<li><a class='dropdown-item' onclick='addItem(\"" . $value['SearchableDesc'] . "\", \"" . $value['SearchableFieldName'] . "\");'>" . $value['SearchableDesc'] . "</a></li>";
									}
								}
								?>
							</ul>
						</div>
					</form>
				</div>
				<div class="col col-sm-2 search text-left">
					<div id="searchResults">
						
					</div>
				</div>
				<div class="col col-sm-3 planSearch" id="planSearch">
						<!--<br><br><br><br>-->
						<!--<span class="loading fa fa-spinner fa-pulse"></span>-->
				</div>
				<div class="col col-sm-1"></div>
			</div>
		</div>
	</body>
</html>
