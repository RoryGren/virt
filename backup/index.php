<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
		<meta name="theme-color" content="#ff9500"/>
		<title>Virtual Architect</title>

		<!-- Boostrap links -->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" media="all" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

		<!-- Website CSS -->
		<link href="style/virtualArchitect.min.css" rel="stylesheet" type="text/css"/>

    </head>
    <body>
		<?php include 'navBarTop.php'; ?>
		<!-- About us page -->
		<div class="home pages" id="home">
			<div class="translucent-background">
				<?php  include 'carouselHome.php'; ?>
			</div>
		</div>

		<!-- About Us page -->
		<div class="aboutUs pages" id="aboutUs">
			<h3 class="text-center">About Us</h3>
		</div>

	<!-- Services page -->
		<div class="services pages" id="services">
		<h3 class="text-center">Our Services</h3>
		<!-- SlideShow for plans -->
		<div class="carouselHolder">
			<div id="carouselIndicator" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators">
					<li data-target="#carouselIndicator" data-slide-to="0" class="active"></li>
					<li data-target="#carouselIndicator" data-slide-to="1"></li>
					<li data-target="#carouselIndicator" data-slide-to="2"></li>
				</ol>
				<div class="carousel-inner">
					<div class="carousel-item active">
						<img class="d-block w-100" src="01.jpg" alt="First slide">
					</div>
					<div class="carousel-item">
						<img class="d-block w-100" src="02.jpg" alt="Second slide">
					</div>
					<div class="carousel-item">
						<img class="d-block w-100" src="03.jpg" alt="Third slide">
					</div>
				</div>
				<a class="carousel-control-prev" data-target="#carouselIndicator" role="button" data-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="carousel-control-next" data-target="#carouselIndicator" role="button" data-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
				</div>
			</div>
		</div>

		<!-- View Plans page -->
		<div class="viewPlans pages" id="searchPlans">
			<h3 class="text-center">Design Your Dream Home</h3>
		</div>

		<!-- Contact Us page -->
		<div class="contactUs pages" id="contactUs">
			<h3 class="text-center">Contact Us</h3>
		</div>

		<p id="result" style="display: none;"></p>

	<!-- Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://use.fontawesome.com/6f7c9f8c3d.js"></script>
	
	<!--<script src="scripts/virtualArchitect.js?eerf=gh32"></script>-->
	<script src="scripts/virtualArchitect.min.js" type="text/javascript"></script>


    </body>
</html>
