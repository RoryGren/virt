<!DOCTYPE html>

<head>
	<!-- Boostrap links -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
	
	<!-- Website CSS -->
	<!--<link rel="stylesheet" type="text/css" href="scripts/virtualArchitect.css?d=4">-->
	<link href="style/virtualArchitect.css" rel="stylesheet" type="text/css"/>

	<title>Virtual Architect</title>
	
</head>

<body>
	<!-- Website navigation -->
	<nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light">
		<a class="navbar-brand" href="#">Virtual Architect</a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>
		<div class="collapse navbar-collapse" id="navbarNav">
			<ul class="navbar-nav">
				<li class="nav-item active">
					<a class="nav-link" href="#home">Home<span class="sr-only">(current)</span></a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#aboutUs">About Us</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#viewPlans">View Plans</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#contactUs">Contact us</a>
				</li>
			</ul>
		</div>
	</nav>

	<!-- About us page -->
	<div class="home pages" id="home">
		&nbsp
	</div>

	<!-- What we do page -->
	<div class="aboutUs pages" id="aboutUs">
		&nbsp
	</div>

	<!-- View Plans page -->
	<div class="viewPlans pages" id="viewPlans">
		<!-- SlideShow for plans -->
		<div class="carouselHolder">
			<div id="carouselIndicator" class="carousel slide" data-ride="carousel">
				<ol class="carousel-indicators">
					<li data-target="#carouselIndicator" data-slide-to="0" class="active"></li>
					<li data-target="#carouselIndicator" data-slide-to="1"></li>
					<li data-target="#carouselIndicator" data-slide-to="2"></li>
				</ol>
				<div class="carousel-inner">
					<div class="carousel-item active">
						<img class="d-block w-100" src="01.jpg" alt="First slide">
					</div>
					<div class="carousel-item">
						<img class="d-block w-100" src="02.jpg" alt="Second slide">
					</div>
					<div class="carousel-item">
						<img class="d-block w-100" src="03.jpg" alt="Third slide">
					</div>
				</div>
				<a class="carousel-control-prev" data-target="#carouselIndicator" role="button" data-slide="prev">
					<span class="carousel-control-prev-icon" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a>
				<a class="carousel-control-next" data-target="#carouselIndicator" role="button" data-slide="next">
					<span class="carousel-control-next-icon" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
				</div>
		</div>
	</div>

	<!-- Contact Us page -->
	<div class="contactUs pages" id="contactUs">
		&nbsp
	</div>

	<p id="result" style="display: none;"></p>


	<!-- Bootstrap JS -->
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

	<!--<script src="scripts/virtualArchitect.js?eerf=gh32"></script>-->
	<script src="scripts/virtualArchitect.js" type="text/javascript"></script>
</body>