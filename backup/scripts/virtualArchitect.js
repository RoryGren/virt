//Adding smooth scrolling
$(document).ready(function(){
    $("a").on('click', function(event) {
        if (this.hash !== "") {
          event.preventDefault();
    
          var hash = this.hash;
    
          $('html, body').animate({
            scrollTop: $(hash).offset().top
          }, 800, function(){
      
            window.location.hash = hash;
          });
        }
    });
});

//Navbar changing active tab
(function ($) {
    'use strict';
    var results = {};

    function display() {
        var resultString = '';

        $.each(results, function (key) {
            resultString += '(' + key + ': ' + Math.round(results[key]) + '%)';
        });

        //$('p').text(resultString);
//        console.log(resultString);
    }

    function calculateVisibilityForDiv(div$) {
        var windowHeight = $(window).height(),
            docScroll = $(document).scrollTop(),
            divPosition = div$.offset().top,
            divHeight = div$.height(),
            hiddenBefore = docScroll - divPosition,
            hiddenAfter = (divPosition + divHeight) - (docScroll + windowHeight);

            if ((docScroll > divPosition + divHeight) || (divPosition > docScroll + windowHeight)) {
                return 0;
            } 
            else {
                var result = 100;

                if (hiddenBefore > 0) {
                    result -= (hiddenBefore * 100) / divHeight;
                }

                if (hiddenAfter > 0) {
                    result -= (hiddenAfter * 100) / divHeight;
                }

                return result;
            }
    }

    function calculateAndDisplayForAllDivs() {
        $('div').each(function () {
            var div$ = $(this);
            results[div$.attr('id')] = calculateVisibilityForDiv(div$);
        });

        display();
    }

    $(document).scroll(function () {
        calculateAndDisplayForAllDivs();
    });

    $(document).ready(function () {
        calculateAndDisplayForAllDivs();
    });
}(jQuery));