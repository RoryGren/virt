<div class="carouselHolder">
	<!-- Indicators -->
	<div id="carouselHome" class="carousel slide" data-ride="carousel">
		<ol class="carousel-indicators">
			<li data-target="#carouselHome" data-slide-to="0" class="active"></li>
			<li data-target="#carouselHome" data-slide-to="1"></li>
			<li data-target="#carouselHome" data-slide-to="2"></li>
			<li data-target="#carouselHome" data-slide-to="3"></li>
			<li data-target="#carouselHome" data-slide-to="4"></li>
		</ol>

		<!-- Wrapper for slides -->
		<div class="carousel-inner" style="height: 80%;">
			<div class="carousel-item active">
				<img class="d-block w-100" src="images/Note3_0(3).png" alt=""/>
			</div>

			<div class="carousel-item">
				<img class="d-block w-100" src="images/Note4_0.png" alt=""/>
			</div>

			<div class="carousel-item">
				<img class="d-block w-100" src="images/Note5_0.png" alt=""/>
			</div>

<!--			<div class="carousel-item">
				<img class="d-block w-100" src="images/E319-3-2.5_C_1_Small.jpg" alt=""/>
			</div>-->

<!--			<div class="carousel-item">
				<img class="d-block w-100" src="images/E319-3-2.5_C_2_Small.jpg" alt=""/>
			</div>-->

		</div>

		<!-- Left and right controls -->
		<a class="carousel-control-prev" data-target="#carouselHome" role="button" data-slide="prev">
		  <span class="carousel-control-prev-icon" aria-hidden="true"></span>
		  <span class="sr-only">Previous</span>
		</a>
		<a class="carousel-control-next" data-target="#carouselHome" role="button" data-slide="next">
		  <span class="carousel-control-next-icon" aria-hidden="true"></span>
		  <span class="sr-only">Next</span>
		</a>
	</div>
</div>